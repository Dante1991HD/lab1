#include <iostream>
using namespace std;

#ifndef ELEMENTO_H
#define ELEMENTO_H

//Se define la clase elemento
class Elemento {
    //Se establece el Número como parámetro privado
    private:
        int numero = 0;
    //Se declaran los método
    public:
        //Constructor por defecto
        Elemento();
    //Método set para el número ingresado
    void set_numero(int numero);
    //Método get retornará el cuadrado del número
    int get_cuadrado();

};
#endif