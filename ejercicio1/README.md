# Ejercicio número 1

## Descripcion del programa 
El programa permite la creación de un arreglo de números enteros, luego, como salida, muestra la suma de los cuadrados de los números ingresados.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

## Compilacion y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando 
```
make
```
* Luego de completada la compilación utilice el siguiente comando
```
./ejercicio1 #
```
Donde va el "#" se debe reemplazar con la cantidad de números enteros que se desea ingresar al arreglo

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
