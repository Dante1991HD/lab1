#include <iostream>
#include "Elemento.h"
using namespace std;

void inicializador(int *arreglo, int n){
    for (int i = 0; i < n; i++) {
        arreglo[i] = i;
    }
}

//Función principal
int main(int argc, char * argv[]) {
    //Se declara el tamaño que tendrá el arreglo, se ingresa por consola
    //Se nombra como "n" a la variable que contiene el tamaño del arreglo
    int n = atoi(argv[1]);
    //Suma contendrá la sumatoria de los cuadrados, digito contiene el valor ingresado por usuario
    int suma = 0, digito;
    //Se declara el arreglo de tamaño "n" anteriormente declarado
    int arreglo[n];
    //Se declara el objeto elemento que contendrá el número entero ingresado por usuario
    Elemento elemento = Elemento();
    //Ciclo principal, este recorre "n" veces pidiendo valores al usuario
    for (int i = 0; i < n; i++) {
        //Se llama a los métodos de elemento para el calculo de la suma de cuadrados
        cout << "Ingrese un numero entero: ";
        cin >> digito;
        elemento.set_numero(digito);
        suma += elemento.get_cuadrado();
    }
    //Se genera la salida de la suma de cuadrados
    cout << "La suma de los cuadrados es: " << suma << endl;

    return 0;
}
