#include <iostream>
#include "Elemento.h"
using namespace std;

//Constructor por defecto
Elemento::Elemento() {
    int numero = 0;
}
//Método para setear el número
void Elemento::set_numero(int numero) {
    this -> numero = numero;
}
//Método que devuelve el cuadrado del número
int Elemento::get_cuadrado() {
    return (this -> numero * this -> numero);
}