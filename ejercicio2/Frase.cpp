#include <iostream>
#include <cctype>
#include "Frase.h"
using namespace std;
//Constructor de la clase
Frase::Frase(){
    string frase = "\0";
    int mayusculas = 0;
    int minusculas = 0;
}

//Metodo para setear la clase
void Frase::set_frase(string frase){
    this->frase = frase;
}

//Metodos get de frase, el total de mayusculas como de minusculas
string Frase::get_frase(){
    return this->frase;
}

int Frase::get_mayusculas(){
     for(int i=0; i<this->frase.size();i++){
        // Ocupamos la funcion isupper
        if(isupper(this->frase[i])){
            // almacenamos la suma
            this->mayusculas = this->mayusculas + 1;
        }
    }
    return this->mayusculas;
}

int Frase::get_minusculas(){
    for(int i=0; i<this->frase.size();i++){
        // En este caso utilizamos la funcion islower
        if(islower(this->frase[i])){
            this->minusculas = this->minusculas + 1;
        }
    }
    return this->minusculas;
}