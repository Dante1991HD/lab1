#include <iostream>
using namespace std;

#ifndef ELEMENTO_H
#define ELEMENTO_H

// Se define la clase de frase
class Frase{
    // Atributos privados de la clase
    private:
        string frase = "\0";
        int mayusculas = 0;
        int minusculas = 0;
    // Atributos publicos
    public:
        // Constructor
        Frase();
        // Metodos set and get además de los metodos para contar mayusculas y minusculas
        void set_frase(string frase);
        string get_frase();
        int get_mayusculas();
        int get_minusculas();
};
#endif
