# Ejercicio número 2

## Descripcion del programa 
El programa permite la creación de un arreglo de frases, a la cual se les calculará la cantidad de mayusculas y minusculas para luego mostrarlo en pantalla.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

## Compilacion y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando 
```
make
```
* Luego de completada la compilación utilice el siguiente comando
```
./ejercicio2 #
```
Donde va el "#" se debe reemplazar con la cantidad de frases que se desea ingresar al arreglo de tipo Frase

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
