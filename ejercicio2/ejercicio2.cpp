#include <iostream>
#include "Frase.h"
using namespace std;


int main(int argc, char* argv[]){
    //Se declara el tamaño que tendrá el arreglo, se ingresa por consola
    //Se nombra como "n" a la variable que contiene el tamaño del arreglo
    int n = atoi(argv[1]);
    //Se declara un arreglo de objetos Frase de tamaño "n"
    Frase frases[n];
    //Se utiliza "Linea" como placeholder para la creacion de las frases
    string linea = "\0";
    for(int i = 0; i < n; i++){
        //Obtenemos la frase
        cout << "Ingrese la frase " << i + 1 << endl;
        getline(cin, linea);
        frases[i] = Frase();
        //Agregamos la frase al arreglo
        frases[i].set_frase(linea);
        cout << endl;
      }
    for(int i = 0; i < n; i++){
        //Impresión final de lo solicitado
        cout << "La frase" << i + 1 << " " << frases[i].get_frase() << " posee: " << endl;
        cout << frases[i].get_mayusculas() << " Mayusculas y ";
        cout << frases[i].get_minusculas() << " Minusculas" << endl;
    }
    return 0;
}
