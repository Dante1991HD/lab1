#include <iostream>
using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

//Se define la clase cliente
class Cliente {
    //Atributos privados de clase
    private:
        string nombre = "\0";
        string telefono = "\0";
        int saldo = 0;
        bool moroso = 0;
    //Atributos publicos de clase
    public:
        //Constructor por defecto
        Cliente();
        //Métodos setter de la clase
        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);
        //Métodos getter de la clase
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();

};
#endif
