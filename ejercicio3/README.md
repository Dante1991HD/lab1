# Ejercicio número 3

## Descripcion del programa 
El programa permite la creación de un arreglo de estructuras de tipo cliente, estas guardan datos tales como el nombre, el número de telefono, el saldo y si el cliente se encuentra moroso o no, para ello pide los datos de manera correlativa y finalmente los muestra en pantalla.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

## Compilacion y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando 
```
make
```
* Luego de completada la compilación utilice el siguiente comando
```
./ejercicio3 #
```
Donde va el "#" se debe reemplazar con la cantidad de clientes que se desea ingresar al arreglo de esta estructura.

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
