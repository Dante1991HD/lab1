#include <iostream>
#include "Cliente.h"
using namespace std;
//Función que muesta en pantalla los datos del cliente
void imprimir_clientes(Cliente * clientes, int largo) {
    cout << "Los clientes ingresados son: " << endl;
    //Ciclo que recorre los datos ingresados y genera una salida por cada uno
    for (int i = 0; i < largo; i++) {
        // Impresion
        cout << "Cliente N°" << i + 1 << endl;
        cout << "Nombre: " << clientes[i].get_nombre() << endl;
        cout << "Telefono: " << clientes[i].get_telefono() << endl;
        cout << "Saldo: " << clientes[i].get_saldo() << endl;
        cout << "El cliente es ";
        cout << (clientes[i].get_moroso() ? "moroso" : "no moroso") << endl;
    }
}
//Función que se encarga de ingresar al arreglo "clientes" los datos de los clientes
void agregar_clientes(Cliente * clientes, int n) {
    string nombre, telefono;
    int saldo, moroso;
    cout << "Ingresar informacion de los clientes" << endl;
    //De manera iterativa se agregaran clientes con todos sus datos 
    //Según la cantidad de clientes a ingresar
    for (int i = 0; i < n; i++) {
        //Se pide a usuario los detos requeridos de los clientes
        cout << "Ingrese el nombre del cliente: ";
        getline(cin, nombre);
        cout << "Ingrese el número del cliente: ";
        getline(cin, telefono);
        cout << "Ingrese el saldo del cliente: ";
        getline(cin, saldo);
        cout << "¿El cliente es moroso? \nIngrese 1 para si \nIngrese 2 para no" << endl;
        getline(cin, moroso);
        clientes[i].set_nombre(nombre);
        clientes[i].set_telefono(telefono);
        clientes[i].set_saldo(stoi(saldo));
        clientes[i].set_moroso(moroso == "1" ? true : false);
    }
}

//Funcion principal
int main(int argc, char * argv[]) {
    //Se declara el tamaño que tendrá el arreglo, se ingresa por consola
    //Se nombra como "n" a la variable que contiene el tamaño del arreglo
    int n = atoi(argv[1]);
    //Creamos un arreglo de tipo Cliente
    Cliente clientes[n];
    //Se llama a la función que llena el arreglo de clientes
    agregar_clientes(clientes, n);
    //Se llama a la función que imprime a los clientes
    imprimir_clientes(clientes, n);

    return 0;
}
